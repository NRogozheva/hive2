hive -f create_tables.sql

SECONDS = 0
hive -f textfile.sql
time_textfile = $SECONDS

SECONDS = 0 
hive -f orc.sql
time_orc = $SECONDS

SECONDS = 0
hive -f parquet.sql
time_parquet = $SECONDS

echo "Data storage format | Query time, sec" > results.md
echo ":---: | :---:" >> results.md
echo "Textfile | $time_textfile" >> results.md
echo "ORC | $time_orc" >> results.md
echo "Parquet | $time_parquet" >> results.md
