ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/json-serde-1.3.8-jar-with-dependencies.jar;

USE rogozhevaan;

DROP TABLE IF EXISTS kkt_table_textfile;
CREATE TABLE kkt__table_textfile
STORED AS TEXTFILE
AS SELECT * FROM kkt_table;

DROP TABLE IF EXISTS kkt_table_orc;
CREATE TABLE kkt_table_orc
STORED AS ORC
AS SELECT * FROM kkt_table;

DROP TABLE IF EXISTS kkt_table_parquet;
CREATE TABLE kkt_table_parquet
STORED AS PARQUET
AS SELECT * FROM kkt_table;
